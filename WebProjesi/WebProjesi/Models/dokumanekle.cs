﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebProjesi.Models
{
    public class dokumanekle
    {
        [Required (ErrorMessage ="Döküman adını giriniz")]
        public string DokumanAdi { get; set; }

        [Required(ErrorMessage = "Völümü giriniz")]
        public string Bolum { get; set; }

        [Required(ErrorMessage = "Sınıfı giriniz")]
        public int Sinif { get; set; }

        public string Dokuman { get; set; }
    }
}